import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HotelService} from '../services/hotel.service';
import {first, takeUntil, tap} from 'rxjs/operators';
import {HotelModel} from '../models/hotel.model';
import {ActivatedRoute} from '@angular/router';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit, OnDestroy {
  hotel: HotelModel;
  hotelId: string;
  destroy$: Subject<boolean> = new Subject();
  myForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private hotelService: HotelService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      name: [null, [Validators.required]],
      roomNumbers: [null, [Validators.required]],
      pool: [null, [Validators.required]]
    });
    this.route.paramMap.subscribe(params => {
      this.hotelId = params.get('id');
    });
  }

  sendform() {
    this.hotel = this.myForm.value;
    this.hotelService.patchNewHotel$(this.hotelId, this.hotel)
      .pipe(
        takeUntil(this.destroy$),
      )
      .subscribe();
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
