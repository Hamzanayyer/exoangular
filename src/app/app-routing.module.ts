import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'hostels', loadChildren: () => import('./list/list.module').then(m => m.ListModule) },
  { path: 'hostels/:id/edit', loadChildren: () => import('./edit/edit.module').then(m => m.EditModule) },
  { path: 'hostels/:id', loadChildren: () => import('./detail/detail.module').then(m => m.DetailModule) },
  { path: '', redirectTo: '/hostels', pathMatch: 'full'},
  { path: 'rooms', loadChildren: () => import('./rooms/rooms.module').then(m => m.RoomsModule) },
  { path: 'hostels/:id/delete', loadChildren: () => import('./delete/delete.module').then(m => m.DeleteModule) }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
