import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HotelModel} from '../models/hotel.model';
import {RoomModel} from '../models/room.model';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HotelService {
  root: string = environment.api;

  constructor(
    private httpClient: HttpClient,
  ) {
  }
  getHotelById$(hotelId: string): Observable<HotelModel> {
    return this.httpClient.get(this.root + 'hostels/' + hotelId) as Observable<HotelModel>;
  }

  getAllHotels$(): Observable<HotelModel[]> {
    return this.httpClient.get(this.root + 'hostels') as Observable<HotelModel[]>;
  }

  getRoomById$(roomId: string): Observable<RoomModel> {
    return (this.httpClient
      .get(this.root + 'rooms/' + roomId) as Observable<RoomModel>);
  }

  createNewHotel$(hotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient
      .post(this.root + 'hostels', hotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
      );
  }
  patchNewHotel$(hotelId: string, hotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient
      .patch(this.root + 'hostels/' + hotelId, hotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
      );
  }

  deleteHotel$(hotelId: string): Observable<string> {
    return (this.httpClient
      .delete(this.root + 'hostels/' + hotelId) as Observable<string>)
      .pipe(
        tap(x => console.log(x)),
      );
  }
}
