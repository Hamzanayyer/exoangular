import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HotelModel} from '../models/hotel.model';
import {HotelService} from '../services/hotel.service';
import {takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {
  @Input() hotel: HotelModel;
  hotelId: string;
  destroy$: Subject<boolean> = new Subject();

  constructor(
    private route: ActivatedRoute,
    private hotelService: HotelService,
  ) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.hotelId = params.get('id');
    });
    this.hotelService.getHotelById$(this.hotelId)
      .pipe(
        tap(element => this.hotel = element),
      )
      .pipe(
        takeUntil(this.destroy$),
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
