export interface RoomModel {
    id: number | string;
    size: number;
    roomName: string;
    roomState: RoomState;
}

export enum RoomState {
    isOccupied = 'est occupé',
    isToSale = 'a vendre',
    isClosed = 'est fermé',
}
