import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {HotelService} from '../services/hotel.service';
import {takeUntil, tap} from 'rxjs/operators';
import {HotelModel} from '../models/hotel.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy{
  hotels: HotelModel[];
  destroy$: Subject<boolean> = new Subject();

  constructor(
    private hotelService: HotelService,
  ) {
  }

  ngOnInit(): void {

    this.hotelService.getAllHotels$()
      .pipe(
        tap(list => this.hotels = list),
      )
      .pipe(
        takeUntil(this.destroy$),
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
